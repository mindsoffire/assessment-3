import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import 'hammerjs';


import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LibService } from './lib.service';




@NgModule({
  declarations: [
    AppComponent,
    routingComponents,

  ],
  imports: [
    BrowserModule, AppRoutingModule, 
    FormsModule, ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [LibService],
  bootstrap: [AppComponent]
})
export class AppModule { }
