import { Component, OnInit } from '@angular/core';
import { analyzeFile } from '@angular/compiler';
import { domRendererFactory3 } from '@angular/core/src/render3/renderer';
import { ElementArrayFinder } from 'protractor';


@Component({
  selector: 'app-photo-gallery',
  templateUrl: './photo-gallery.component.html',
  styleUrls: ['./photo-gallery.component.css']
})
export class PhotoGalleryComponent implements OnInit {
  constructor() { }


  tinderCeleb = [
    {
      id: 1,
      name: 'Sandra',
      pic: 'https://www.hellomagazine.com/imagenes/profiles/sandra-bullock/5777-sandra-bullock.jpg',
      likes: 0,
      dislikes: 0,
      show: false,
      likeOrDislikePressed: 0
    },
    {
      id: 2,
      name: 'Jennifer',
      pic: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/JenniferAnistonHWoFFeb2012.jpg/800px-JenniferAnistonHWoFFeb2012.jpg',
      likes: 0,
      dislikes: 0,
      show: false,
      likeOrDislikePressed: 0
    },
    {
      id: 3,
      name: 'Brad',
      pic: 'https://www.thesun.co.uk/wp-content/uploads/2017/06/nintchdbpict000283678777.jpg?strip=all&w=640',
      likes: 0,
      dislikes: 0,
      show: false,
      likeOrDislikePressed: 0
    },
    {
      id: 4,
      name: 'Hugh',
      pic: 'https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cg_face%2Cq_80%2Cw_300/MTE1ODA0OTcxOTA3NTgxNDUz/hugh-jackman-16599916-1-402.jpg',
      likes: 0,
      dislikes: 0,
      show: false,
      likeOrDislikePressed: 0
    }
  ];
  totNumLikes = 0;
  totNumDislikes = 0;
  randomizeTimes;

  // AJ bcareful of huge while loops in init or anywhere
  ngOnInit() {
    this.randomizeTimes = Math.floor(Math.random() * 3 + 1);
    let i = 1;
    while (i <= this.randomizeTimes) {
      let numRet = Math.floor(Math.random() * this.tinderCeleb.length);
      (this.randomizeTimes == 1) ? numRet : this.randomizeTimes;
      if (this.tinderCeleb[numRet].show != true) {
        i++;
        this.tinderCeleb[numRet].show = true;
      }
    }
  }




  onLikeEvt(event) {
    console.log(event);
    let retID = event.id - 1;
    this.totNumLikes++;

    this.tinderCeleb[retID].likeOrDislikePressed = event.likeOrDislikePressed;
    if (this.tinderCeleb[retID].likeOrDislikePressed == 1) {
      //this.totNumLikes++;
      this.tinderCeleb[retID].likes++;
      this.tinderCeleb[retID].likeOrDislikePressed == 0;
      if(this.tinderCeleb[retID].likes == 18) this.loadNextCeleb();
     }
  }

  onDisLikeEvt(event) {
    console.log(event);
    let retID = event.id - 1;
    this.totNumDislikes++;

    this.tinderCeleb[retID].likeOrDislikePressed = event.likeOrDislikePressed;
    if (this.tinderCeleb[retID].likeOrDislikePressed == 2) {
      //this.totNumDislikes++;
      this.tinderCeleb[retID].dislikes++;
      this.tinderCeleb[retID].likeOrDislikePressed == 0;
    }
  }

  loadNextCeleb() : void {
    let foundone = false;
    for(let i=0; i<this.tinderCeleb.length || !foundone; i++){
      if(this.tinderCeleb[i].show == false && this.tinderCeleb[i].likes == 0){
        this.tinderCeleb[i].show == true;
        foundone = true;
      }
    }
  }


}


