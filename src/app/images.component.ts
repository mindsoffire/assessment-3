import { Component, OnInit } from '@angular/core';

/* import * as $ from 'jquery';
import { JsonPipe } from '@angular/common'; */

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  localUrl: any[];
  picSrc: string = "";

  showPreviewImage(event: any, fileToSend: string) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = event.target.result;
      }
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  postImage(fileToSend: string) {
    this.picSrc = `file:///${fileToSend.replace(/\\/gim, "/")}`;
    console.info(this.picSrc);
  }
}
