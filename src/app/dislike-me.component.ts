import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RetCeleb } from './celeb';

@Component({
  selector: 'app-dislike-me',
  templateUrl: './dislike-me.component.html',
  styleUrls: ['./dislike-me.component.css']
})
export class DislikeMeComponent implements OnInit {
  @Input() celebID : number;
  @Output() onDisLikeEvt = new EventEmitter<RetCeleb>();
  likeCelebFlag: any;
  constructor() { }

  ngOnInit() {
  }

  

  dislike(celebID){
    this.likeCelebFlag = new RetCeleb(celebID, 2);
    //this.likeCelebFlag.likeOrDislikePressed = 2;
    //this.likeCelebFlag.id = celebID;
    this.onDisLikeEvt.emit(this.likeCelebFlag);
  }


}
