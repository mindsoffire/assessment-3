import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RetCeleb } from './celeb';

@Component({
  selector: 'app-like-me',
  templateUrl: './like-me.component.html',
  styleUrls: ['./like-me.component.css']
})
export class LikeMeComponent implements OnInit {
  @Input() celebID: number;
  @Output() onLikeEvt = new EventEmitter<RetCeleb>();
  likeCelebFlag: any;
  constructor() { }

  ngOnInit() {
  }

  like(celebID) {
    this.likeCelebFlag = new RetCeleb(celebID, 1);
    this.onLikeEvt.emit(this.likeCelebFlag);
  }

}

