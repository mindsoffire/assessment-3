import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LibService {

  readonly SERVER = 'https://ajlib2.localtunnel.me';

  cursor = 0;
  title = '';
  name = '';
  showcaseTitle = 'BROWSE BOOKS';
  
  constructor(private httpClient: HttpClient) { }

  // GET /book/<bookId>
  getBook(bookId: number): Promise<any> {
    return (
      this.httpClient.get(`${this.SERVER}/book/${bookId}`)
        .take(1).toPromise()
    );
  }

  // GET /books?title=<n>&name=<n>&limit=<n>&offset=<n>
  getAllBooks(config = {}): Promise<any> {
    //Set the query string
    let qs = new HttpParams();
    qs = qs.set('title', config['title'] || '')
      .set('name', config['name'] || '')
      .set('limit', config['limit'] || 10)
      .set('offset', config['offset'] || 0);
    return (
      this.httpClient.get(`${this.SERVER}/books`, { params: qs })
        .take(1).toPromise()
    );
  }


}
