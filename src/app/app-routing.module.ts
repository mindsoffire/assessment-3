import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ImagesComponent } from './images.component';
import { BooksComponent } from './books.component';
import { BookDetailsComponent } from './book-details.component';
import { ShopCartComponent } from './shop-cart.component';
import { PhotoGalleryComponent } from './photo-gallery.component';
import { LikeMeComponent } from './like-me.component';
import { DislikeMeComponent } from './dislike-me.component';

import { KycformComponent } from './kycform.component';
import { WealthRegistrationComponent } from './wealth-registration/wealth-registration.component';

const routes: Routes = [
    { path: '', component: BooksComponent },
    { path: 'images', component: ImagesComponent },
    { path: 'celeblikes', component: PhotoGalleryComponent },
    { path: 'shop-cart', component: ShopCartComponent },
    { path: 'books', component: BooksComponent },
    { path: 'books/:bookID', component: BookDetailsComponent },
/*     { path: 'wealthFi/KYC', component: KycformComponent }, */
    { path: 'wealthFi/KYC', component: WealthRegistrationComponent },
    { path: '**', redirectTo: '/images', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ImagesComponent, ShopCartComponent,
    BooksComponent, BookDetailsComponent,
    PhotoGalleryComponent, LikeMeComponent, DislikeMeComponent, 
    KycformComponent, WealthRegistrationComponent ]