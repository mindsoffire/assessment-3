// load libraries needed.
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
app.use(cors());
// app.use(bodyParser.json()); // for parsing application/json

var sha256 = require('js-sha256').sha256;

// instance of filesys.
fs = require('fs');

/* function rotaj(s) {
    return s.replace(/[A-Za-z0-9]/g, function (c) {
        return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890".charAt(
            "MNBVCXZasdfghjklPOIUYTREWQASDFGHJKLmnbvcxzpoiuytrewq6172839405".indexOf(c)
        );
    });
} */
function rotja(s) {
    return s.replace(/[A-Za-z0-9]/g, function (c) {
        return "MNBVCXZasdfghjklPOIUYTREWQASDFGHJKLmnbvcxzpoiuytrewq6172839405".charAt(
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890".indexOf(c)
        );
    });
}

SERVER_SALT = 'clntUsrKYC+NONCE:JesusChristIsDivineKingOfAllSecularKings-SaltOfTheEarthAJ4TCYAndrewJohnJoshuaJamesJeremiahTanChoonYewIM2CNSIrisMaryMariaChuangNaiShanLITWSLucasIsaacTanWeiShuenLJTWSLinusJosephTanWeiShwinLATTWCLeoAugustineThomasTanWeiCher2018';
var found = false;
var fillClntUserKYC = {
    encEmailID: null,
    encPW: null,
    subDate: null,
    clntCreatDate: null,
    clntValKYCDate: null,
    legalName: [],
    legalIDCred: null,
    photo: {
        photoIDFrontURL: null,
        photoIDBackURL: null,
        photoIDsDated: null,
        photoValNowIDURL: null,
        photoValNowIDDated: null
    },
    gender: null,
    DOB: null,
    nationty: null,
    address: {
        postCode: null,
        postStreet: null,
        postBlock: null,
        postUnit: null
    },
    encMobileNum: null,
    bank: {
        bankName: null,
        bankScanStatemtURL: null,
        bankScanStatemtURLDated: null,
        bankScanStatemtBal: null,
        bankAcct: null,
    },
    textAnnotNotaryOthers: null,
    clntNotes: null,

    contents: [],
    savedTime: '',
    serverStatusCode: null
}
toRegister = { emailID: 'Please sign up first.', contents: [], savedTime: 'You have not signed up!' };
// npm run main.js 6000 ; nodemon main.js 5000 ; node main.js 4000
app.set('port', process.argv[2] || process.env.PORT || 3000);
var server = app.listen(app.get('port'), () => {
    console.log('Express server listening at http://(' + server.address().family + ')127.0.0.1:' + server.address().address + server.address().port);
});

/* let clntUserKYC = require('./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json');
console.log(JSON.stringify(clntUserKYC));
 */

app.get('/api/clntUserKYC', (req, res) => {
    let clntUserKYC = require('./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json');
    /*     console.log(JSON.stringify(clntUserKYC)); */
    console.log('app.get: query.eID, query.signU, query.logI', req.query.eID, req.query.signU, req.query.logI);
    var toJSON = {};

    found = false;

    for (let [index, c] of clntUserKYC.entries()) {
        if (req.query.eID == c.encEmailID) {
            toJSON = c;
            found = true;
            if (req.query.logI != 'false') {
                // for logIn option chosen at client.
                if (!c.clntCreatDate) {
                    console.log('You have not signed up yet!');
                    toJSON.serverStatusCode = 'You have not signed up yet!';
                }
                else if (c.clntCreatDate && !c.clntValKYCDate && req.query.logI == c.encPW) {
                    // return toJSON = c done.
                    console.log('You have logged in and here\'s your unfinished form!');
                    toJSON.serverStatusCode = 'You have logged in and here\'s your unfinished form!';
                }
                else if (c.clntCreatDate && c.clntValKYCDate && req.query.logI == c.encPW) {
                    console.log('Congrats, your account has been approved to transfer in funds..');
                    toJSON.serverStatusCode = 'Congrats, your account has been approved to transfer in funds..';
                    /*                     fs.writeFile(`assets/${sha256(req.query.eID + req.query.logI + SERVER_SALT)}.json`, JSON.stringify(clntUserKYC), 'utf8', (err) => {
                                            if (err) {
                                                console.log("An error occured while writing new asset JSON table.");
                                                console.log(err);
                                                console.log(data);
                                                return res.status(500).json({ 'new client-user asset-table file write error': err });
                                            }
                                            console.log("New asset JSON Object created.");
                                        }); */
                    let toJSONAdd = require(`./assets/${sha256(req.query.eID + req.query.logI + SERVER_SALT)}.json`);
                    console.log('toJSONAdd: ', toJSONAdd);
                    toJSON.push(toJSONAdd);
                }
                else if (req.query.logI != c.encPW) {
                    console.log('Your password is not correct..pls try again!');
                    toJSON = {};
                    toJSON.serverStatusCode = 'Your password is not correct..pls try again!';
                    /*     toJSON = { "appError": "Your password is not correct..pls try again!" }; */
                }
            }
            if (req.query.signU != 'false') {
                // for signUp option chosen at client.
                if (c.clntCreatDate) {
                    if (req.query.signU == c.encPW) {
                        console.log('You have already signed up an account with us..');
                        toJSON.serverStatusCode = 'You have already signed up an account with us..and so we\'re logging you in..';
                        // respond toJSON = c done.
                    }
                    else {
                        console.log('This logIn ID has already been used..');
                        toJSON = {};
                        toJSON.serverStatusCode = 'This logIn ID has already been used..';
                        /*      toJSON = { "appError": "This logIn ID has already been used.." }; */
                    }
                }
                else {
                    // respond toJSON with fillClntUserKYC form.
                    fillClntUserKYC.encEmailID = req.query.eID;
                    fillClntUserKYC.encPW = req.query.signU;
                    fillClntUserKYC.subDate = c.subDate;
                    fillClntUserKYC.clntCreatDate = new Date();
                    c = fillClntUserKYC;
                    /*                     c.newTryDude = 'Dude';
                                        console.log('index of c: ', index);
                                          clntUserKYC[index].hey = "there11"; */
                    clntUserKYC[index] = fillClntUserKYC;
                    /*      console.log('c>> ', c); */
                    console.log('clntUserKYC>> ', clntUserKYC);
                    fs.writeFile("./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json", JSON.stringify(clntUserKYC), 'utf8', (err) => {
                        if (err) {
                            console.log("An error occured while writing new subscriber JSON Object to masterIndexHead.");
                            console.log(err);
                            console.log(data);
                            return res.status(500).json({ 'write to masterIndexHead error': err });
                        }
                        console.log("New signUp JSON Object saved to masterIndexHead.");
                    });
                    toJSON = fillClntUserKYC;
                    toJSON.serverStatusCode = 'Thank you for signing up for our service..';
                }
            }
            if (req.query.logI == 'false' && req.query.signU == 'false') {
                if (!c.subDate) {
                    let writeNewSub = {};
                    writeNewSub.encEmailID = req.query.eID;
                    writeNewSub.subDate = new Date();
                    console.log('writeNewSub>> ', writeNewSub);
                    toJSON = writeNewSub;
                    fs.writeFile(`./ ${writeNewSub.encEmailID}.json`, JSON.stringify(writeNewSub), { flag: 'w' }, (err) => {
                        if (err)
                            return console.error(err);
                        fs.readFile(`./ ${writeNewSub.encEmailID}.json`, 'utf-8', (err, data) => {
                            if (err) {
                                console.error(err);
                                console.log(data);
                                return res.status(500).json({ 'cannot subscribe error': err });
                            }
                        });
                    });
                    c.subDate = writeNewSub.subDate;
                    fs.writeFile("./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json", JSON.stringify(clntUserKYC), 'utf8', (err) => {
                        if (err) {
                            console.log("An error occured while writing new subscriber JSON Object to masterIndexHead.");
                            console.log(err);
                            console.log(data);
                            return res.status(500).json({ 'write to masterIndexHead error': err });
                        }
                        console.log("New subscriber JSON Object saved to masterIndexHead.");
                    });
                    toJSON.serverStatusCode = 'Thank you for subscribing..';
                }
                else {
                    console.log('You have already subscribed!')
                    toJSON = require(`./ ${req.query.eID}.json`);
                    toJSON.serverStatusCode = 'You have already subscribed!';
                }
            }
        }
        else {
        }
    }
    if (!found) {
        let writeNewSub = {};
        writeNewSub.encEmailID = req.query.eID;
        writeNewSub.subDate = new Date();
        console.log('writeNewSub>> ', writeNewSub);
        toJSON = writeNewSub;
        fs.writeFile(`./ ${writeNewSub.encEmailID}.json`, JSON.stringify(writeNewSub), { flag: 'w' }, (err) => {
            if (err)
                return console.error(err);
            fs.readFile(`./ ${writeNewSub.encEmailID}.json`, 'utf-8', (err, data) => {
                if (err) {
                    console.error(err);
                    console.log(data);
                    return res.status(500).json({ 'cannot subscribe error': err });
                }
            });
        });
        clntUserKYC.push(writeNewSub);
        fs.writeFile("./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json", JSON.stringify(clntUserKYC), 'utf8', (err) => {
            if (err) {
                console.log("An error occured while writing new subscriber JSON Object to masterIndexHead.");
                console.log(err);
                console.log(data);
                return res.status(500).json({ 'write to masterIndexHead error': err });
            }
            console.log("New subscriber JSON Object saved to masterIndexHead.");
        });
        toJSON.serverStatusCode = 'Thank you for subscribing..';
    }


    /*    if (req.query.signU == 'false') {
           (!found) ? toJSON = toRegister : toJSON;
       }
       else if (req.query.signU == 'true') {
           if (!found) {
               fill.emailID = req.query.eID;
               fill.savedTime = 'Thank you for signing up!';
               userCarts.push(fill);
               fs.writeFile("./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json", JSON.stringify(userCarts), 'utf8', (err) => {
                   if (err) {
                       console.log("An error occured while writing JSON Object to File.");
                       return console.log(err);
                   }
   
                   console.log("JSON file has been saved.");
               });
               toJSON = fill;
           }
           else {
               fill.emailID = req.query.eID;
               fill.savedTime = 'You have registered already!';
               toJSON = fill;
           }
       } */
    //(toJSON)? toJSON : toJSON = toRegister;
    console.log('toJSON before end app.get: ', toJSON)
    console.log('toJSON.serverStatusCode: ', toJSON.serverStatusCode)
    res.status(200).json(toJSON);
});
/* app.post('/api/clntUserKYC', bodyParser.json(), (req, res) => {
    let clntUserKYC = require('./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json');
    var toJSON = null;
    found = false;
    console.log(req.body);
    for (let c of clntUserKYC) {
        if (req.body.emailID == c.encEmailID) {
            i.contents = req.body.contents;
            i.savedTime = new Date();
            console.log(i);
            toJSON = i;
            found = true;
        }
    }

    fs.writeFile("./a02e2d007ea7dbf860cec8b48aa5eeccd97492d48f9a59b67c8139b9267f6dad.json", JSON.stringify(userCarts), 'utf8', (err) => {
        if (err) {
            console.log("An error occured while writing JSON Object to File.");
            return console.log(err);
        }

        console.log("JSON file has been saved.");
    });
    (!found) ? toJSON = toRegister : toJSON;
    res.status(200).json(toJSON);
}); */

app.use(express.static(__dirname + '/public'));
app.use((req, res, next) => {
    res.redirect('/error.html');
});
