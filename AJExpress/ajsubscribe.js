// load libraries needed
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mysql = require('mysql');

// instantiate express-application obj
const app = express();
// use CORS middleware
app.use(cors());

// e.g. npm run main.js 6000 ; nodemon main.js 6500 ; node main.js 6500 ; nodemon cart.js 6501
app.set('port', process.argv[2] || process.env.PORT || 3000);

var server = app.listen(app.get('port'), () => {
    console.log('Express server listening at http://(' + server.address().family +
        ')127.0.0.1:' + server.address().address + server.address().port); // to find what y server.address().address don't work
});

function sanitizeString(str) {
    str = str.replace(/[^a-z0-9áéíóúñü .-]/gim, "");  /* str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim,""); */
    return str.trim();
}

var lib = require('./lib.json');

console.log(lib[2].author_firstname + ' ' + lib[3].author_lastname);
console.log(lib.length);
lib[126] = lib[1];
console.log(lib[125]);
console.log(lib[126]);
/* console.log(JSON.stringify(lib)); */



var fs = require('fs');
var stream = fs.createWriteStream("my_file.txt");
stream.once('open', function (fd) {
    stream.write("My first row\r\n");
    stream.write("My second row\r\n");
    stream.end();
});

const { promisify } = require('util')
/* const fs = require('fs') */
const readFileAsync = promisify(fs.readFile)
var obj = {};
readFileAsync('./lib.json', { encoding: 'utf8' })
    .then(contents => {
        obj = JSON.parse(contents);
        obj[125] = obj[0];
        obj[126] = obj[1];
        obj[127] = obj[2];
        console.log(JSON.stringify(obj));
    })
    .catch(error => {
        throw error
    })
// obj[125] = obj[1]; not accesssible outide readFileAsync


const msg = '{"name1":"John", "age":"22"}';

var jsonObj = JSON.parse(msg);
console.log(jsonObj);

// convert JSON object to String
var jsonStr = JSON.stringify(jsonObj);
console.log(jsonStr);

// read json string to Buffer
const buf = Buffer.from(jsonStr); console.log(buf.length);
var libStr = JSON.stringify(lib[126]); console.log(libStr);
const buff = Buffer.from(libStr); console.log(buff);
/* fs.writeFile('./lib1.json', JSON.stringify(obj), 'utf8', (err) => {
    if (err) throw err;
    console.log("The file was saved!");
}); */

// stringify JSON Object
/* var jsonContent = JSON.stringify(lib);
console.log(jsonContent); */

/* fs.writeFile("./output.json", jsonContent, 'utf8', function (err) {
    if (err) {
        console.log("An error occured while writing JSON Object to File.");
        return console.log(err);
    }
 
    console.log("JSON file has been saved.");
}); */


// load MySQL config file
const mySQLConfig = require('./mysql-lib-config');
const pool = mysql.createPool(mySQLConfig);

// call mkQuery to handle SQL API
const mkQuery = (SQL, pool) => {
    return (params) => {
        const p = new Promise((resolve, reject) => {
            console.log("In closure: ", SQL);
            pool.getConnection((err, conn) => {
                if (err) {
                    reject({ status: 500, error: err }); return;
                    // return a reject to getConnection from pool
                }
                conn.query(SQL, params || [],
                    (err, result) => {
                        try {
                            if (err)
                                reject(err);
                            // reject query bcos of error
                            else {
                                console.info(result);
                                /* reject({ status: 400, error: err }); return; */
                                resolve(result);
                            }
                        } finally {
                            conn.release();
                        }
                    }
                )
            })
        })
        return (p); // return Promise to params callback
    }               // return a params function 
}

// set up route & handler for getting all books loaded
/* const SELECT_BOOKS = `select id, author_firstname, author_lastname, title, cover_thumbnail from books order by title asc limit ? offset ?`;
const selectBooks = mkQuery(SELECT_BOOKS, pool);   // instantiate params function object that closed mkQuery
app.get('/books', (req, res) => {
    const limit = req.query.limit || 10;
    const offset = req.query.offset || 0;

    selectBooks([parseInt(limit), parseInt(offset)])
        .then(result => { console.log(result); res.status(200).json(result); })
        .catch(err => { });
}); */

const SELECT_BOOKS_N_BY_SEARCH = `select id,  author_firstname, author_lastname, title, cover_thumbnail from books where title like ? && (author_firstname like ? || author_lastname like ?) order by title asc limit ? offset ?`;
const selectBooks = mkQuery(SELECT_BOOKS_N_BY_SEARCH, pool);   // instantiate params function object that closed mkQuery
app.get('/books', (req, res) => {

    cleanStr = sanitizeString(req.query.title)
    const title = cleanStr.split(/[, ]+/);
    /* const title = req.query.title.split(/[, ]+/); */

    cleanStr = sanitizeString(req.query.name)
    const name = cleanStr.split(/[, ]+/);
    /* const name = req.query.name.split(/[, ]+/); */
    const limit = req.query.limit || 10;
    const offset = req.query.offset || 0;

    title[0] = '%' + title[0] + '%';
    (name.length == 1) ? name[1] = name[0] : name[1];
    fname = '%' + name[0] + '%';
    lname = '%' + name[1] + '%';

    console.info('>> title[0]:', title[0]);
    console.info('>> fname:', fname);
    console.info('>> lname:', lname);

    selectBooks([title[0], fname, lname, parseInt(limit), parseInt(offset)])
        .then(result => { console.log(result); res.status(200).json(result); })
        .catch(err => { });
});

// route & handler for selecting book by id
const SELECT_BY_BOOK_ID = 'select * from books where id = ?';
const selectByBookId = mkQuery(SELECT_BY_BOOK_ID, pool);
app.get('/book/:bookId', (req, res) => {
    console.log(`book id = ${req.params.bookId}`);

    selectByBookId([req.params.bookId])
        .then(result => {
            if (result.length)
                res.status(200).json(result[0]);
            else
                res.status(404).json({ error: "Not found" });
        });
});

app.use(express.static(__dirname + '/public'));

app.use((req, res, next) => {
    res.redirect('/error.html');
});