// load libraries needed
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mysql = require('mysql');
var sha256 = require('js-sha256').sha256;
const app = express();
var asciify = require('asciify-image');
var fileUpLoad = require('express-fileupload');
//var imageToAscii = require('image-to-ascii');

var PDFDoc = require('pdfkit');


app.use(fileUpLoad());
//app.use(multer());

// instantiate express-application obj

// use CORS middleware
app.use(cors());

// e.g. npm run main.js 6000 ; nodemon main.js 6500 ; node main.js 6500 ; nodemon cart.js 6501
app.set('port', process.argv[2] || process.env.PORT || 3000);

var server = app.listen(app.get('port'), () => {
    console.log('Express server listening at http://(' + server.address().family +
        ')127.0.0.1:' + server.address().address + server.address().port); // to find what y server.address().address don't work
    console.log(sha256('mindsoffire7@8gmail.com'));
});

function sanitizeString(str) {
    str = str.replace(/[^a-z0-9áéíóúñü .-]/gim, "");  /* str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim,""); */
    return str.trim();
}

var lib = require('./lib.json');


console.log(lib[2].author_firstname + ' ' + lib[3].author_lastname);
console.log(lib.length);
lib[125] = lib[1];
lib[125].author_lastname = lib[0].author_lastname;
lib[126] = lib[1];
lib[127] = lib[2];
console.log(lib[125]);
console.info(lib.length);
//console.log(lib[126]);
/* console.log(JSON.stringify(lib)); */



var fs = require('fs');
var stream = fs.createWriteStream("goodfile.json");
stream.once('open', function (fd) {
    stream.write("My first row\r\n");
    stream.write("My second row\r\n");
    stream.write(JSON.stringify(lib));
    stream.end();
});

fs.writeFile("lib.json", JSON.stringify(lib), 'utf8', (err) => {
    if (err) {
        console.log("An error occured while writing JSON Object to File.");
        return console.log(err);
    }
 
    console.log("JSON file has been saved.");
});

const { promisify } = require('util');

const readFileAsync = promisify(fs.readFile);
const fs1 = require('fs');

var obj = {};
readFileAsync('./lib.json', { encoding: 'utf8' })
    .then(contents => {
        obj = JSON.parse(contents);
        obj[125] = obj[0];
        obj[126] = obj[1];
        obj[127] = obj[2];
        obj[127].author_lastname = obj[126].author_lastname;
        console.log(JSON.stringify(obj));

    })
    .catch(error => {
        throw error
    });
// obj[125] = obj[1]; not accesssible outide readFileAsync


const msg = '{"name1":"John", "age":"22"}';

var jsonObj = JSON.parse(msg);
console.log(jsonObj);

// convert JSON object to String
var jsonStr = JSON.stringify(jsonObj);
console.log(jsonStr);

// read json string to Buffer
const buf = Buffer.from(jsonStr); console.log(buf.length);
var libStr = JSON.stringify(lib[125]); console.log(libStr);
const buff = Buffer.from(libStr); console.log(buff);
/* fs.writeFile('./lib1.json', JSON.stringify(obj), 'utf8', (err) => {
    if (err) throw err;
    console.log("The file was saved!");
}); */

// stringify JSON Object
/* var jsonContent = JSON.stringify(lib);
console.log(jsonContent); */

/* fs.writeFile("./output.json", jsonContent, 'utf8', function (err) {
    if (err) {
        console.log("An error occured while writing JSON Object to File.");
        return console.log(err);
    }
 
    console.log("JSON file has been saved.");
}); */


// load MySQL config file
const mySQLConfig = require('./mysql-lib-config');
const pool = mysql.createPool(mySQLConfig);

// call mkQuery to handle SQL API
const mkQuery = (SQL, pool) => {
    return (params) => {
        const p = new Promise((resolve, reject) => {
            console.log("In closure: ", SQL);
            pool.getConnection((err, conn) => {
                if (err) {
                    reject({ status: 500, error: err }); return;
                    // return a reject to getConnection from pool
                }
                conn.query(SQL, params || [],
                    (err, result) => {
                        try {
                            if (err)
                                reject(err);
                            // reject query bcos of error
                            else {
                                console.info(result);
                                /* reject({ status: 400, error: err }); return; */
                                resolve(result);
                            }
                        } finally {
                            conn.release();
                        }
                    }
                )
            })
        })
        return (p); // return Promise to params callback
    }               // return a params function 
}

// set up route & handler for getting all books loaded
/* const SELECT_BOOKS = `select id, author_firstname, author_lastname, title, cover_thumbnail from books order by title asc limit ? offset ?`;
const selectBooks = mkQuery(SELECT_BOOKS, pool);   // instantiate params function object that closed mkQuery
app.get('/books', (req, res) => {
    const limit = req.query.limit || 10;
    const offset = req.query.offset || 0;

    selectBooks([parseInt(limit), parseInt(offset)])
        .then(result => { console.log(result); res.status(200).json(result); })
        .catch(err => { });
}); */

const SELECT_BOOKS_N_BY_SEARCH = `select id,  author_firstname, author_lastname, title, cover_thumbnail from books where title like ? && (author_firstname like ? || author_lastname like ?) order by title asc limit ? offset ?`;
const selectBooks = mkQuery(SELECT_BOOKS_N_BY_SEARCH, pool);   // instantiate params function object that closed mkQuery
app.get('/books', (req, res) => {

    cleanStr = sanitizeString(req.query.title)
    const title = cleanStr.split(/[, ]+/);
    /* const title = req.query.title.split(/[, ]+/); */

    cleanStr = sanitizeString(req.query.name)
    const name = cleanStr.split(/[, ]+/);
    /* const name = req.query.name.split(/[, ]+/); */
    const limit = req.query.limit || 10;
    const offset = req.query.offset || 0;

    title[0] = '%' + title[0] + '%';
    (name.length == 1) ? name[1] = name[0] : name[1];
    fname = '%' + name[0] + '%';
    lname = '%' + name[1] + '%';

    console.info('>> title[0]:', title[0]);
    console.info('>> fname:', fname);
    console.info('>> lname:', lname);

    selectBooks([title[0], fname, lname, parseInt(limit), parseInt(offset)])
        .then(result => { console.log(result); res.status(200).json(result); })
        .catch(err => { });
});

// route & handler for selecting book by id
const SELECT_BY_BOOK_ID = 'select * from books where id = ?';
const selectByBookId = mkQuery(SELECT_BY_BOOK_ID, pool);
app.get('/book/:bookId', (req, res) => {
    console.log(`book id = ${req.params.bookId}`);

    selectByBookId([req.params.bookId])
        .then(result => {
            if (result.length)
                res.status(200).json(result[0]);
            else
                res.status(404).json({ error: "Not found" });
        });
});

app.get('/images/ascii', (req, res) => {
    const asciiPath = __dirname + '/public' + req.originalUrl;
    console.log(asciiPath + '.html for get');
    res.status(200).sendFile(asciiPath + '.html');
});

app.post('/images/ascii', (req, res) => {
    const asciiPath = __dirname + '/public' + req.originalUrl;
    console.log(asciiPath + '.html for post');
    var options = {
        fit: 'box',
        width: 200,
        height: 100
    }

    if (!req.files.img) {
        return res.status(400).send('No image was uploaded.');
    }
    let imageFile = req.files.img;
    console.log(asciiPath + '.jpg');
    imageFile.mv(asciiPath + '.jpg', (err) => {
        if (err) {
            return res.status(500).send(err);
        }
        //res.status(200).sendFile(asciiPath + '.jpg');       

    });
    asciify(asciiPath + '.jpg', options, (err, asciified) => {
        if (err) console.log(err);
        console.log(asciified);
        fs.writeFileSync(asciiPath + '.txt', asciified, 'utf-8');
        // write data to file sample.html
        fs.writeFile(asciiPath + '.asc', asciified, 'ascii',
            (err) => {
                if (err) throw err;
                // if no error
                console.log("Data is written to file successfully.")
            });
        //res.status(200).set('Content-Type', 'text/plain').sendFile(asciiPath + '.txt');
        doc = new PDFDoc;
        doc.fontSize(8)
            .font('Courier')
            .text(asciiPath + '.pdf', { width: 200, align: 'justify' })
            .image(asciiPath + '.jpg', { width: 200 })
            .pipe(fs.createWriteStream(asciiPath + '.pdf'))
            .on('finish', () => {
                console.log('PDF closed')
            });
        //doc.pipe(res);
        doc.pipe(res);
        doc.end();
        //res.status(200).sendFile(asciiPath + '.pdf');

    });

    /*     imageToAscii(asciiPath + '.jpg', { color: true }, (err, converted) => {
            console.error(err || converted);
        }); */

});

app.get('/images', (req, res) => {
    const catdog = [
        { id: 1, name: 'cat1.png' },
        { id: 2, name: 'cat2.png' },
        { id: 3, name: 'cat3.png' },
        { id: 4, name: 'dog1.jpg' },
        { id: 5, name: 'dog2.jpg' },
        { id: 6, name: 'dog3.jpg' },
    ];
    var typeOfPic;
    var pathToImg;

    let numRet = Math.floor(Math.random() * catdog.length);

    (catdog[numRet].name.endsWith("jpg")) ?
        typeOfPic = "jpg" : typeOfPic = "png";

    var options = {
        fit: 'box',
        width: 200,
        height: 100
    }

    pathToImg = __dirname + '/public' + req.originalUrl + '/' + catdog[numRet].name;
    /*     console.log(pathToImg);
        asciify(pathToImg, options, function (err, asciified) {
            if (err) throw err;
            console.log(asciified);
        }); */


    res.status(200)

    res.format({

        'text/plain': () => {
            asciify(pathToImg, options, function (err, asciified) {
                if (err) throw err;
                console.log(asciified);
                res.send(asciified);
                //res.sendFile(__dirname +'/public' + req.originalUrl + '/' + asciified);
            });
        },

        'text/html': () => {
            res.send(`<h1>The time is ${(new Date())}
                            and a random image of type ${typeOfPic} is found at src="${req.originalUrl}/${catdog[numRet].name}"</h1><img src="${req.originalUrl}/${catdog[numRet].name}">`);
        },

        'image/jpg': () => {
            //console.log(res.get('Content-Type'));
            res.sendFile(__dirname + '/public' + req.originalUrl + '/' + catdog[numRet].name);
        },

        'image/png': () => {
            //console.log(res.get('Content-Type'));
            res.sendFile(__dirname + '/public' + req.originalUrl + '/' + catdog[numRet].name);
        },

        'application/json': () => {
            res.json({ typeOfPic: typeOfPic, picUrl: req.originalUrl + '/' + catdog[numRet].name });
        },

        'default': () => {
            res.status(406).end('Cannot return media type: ' + req.get('Accept'));
        }
    });

});

app.use(express.static(__dirname + '/public'));


app.use((req, res, next) => {
    res.redirect('/error.html');
});