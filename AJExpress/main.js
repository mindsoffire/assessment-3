// var debug = require('debug');
const express = require('express');
//const multer = require('multer');
var asciify = require('asciify-image');
var fileUpLoad = require('express-fileupload');
//var imageToAscii = require('image-to-ascii');
var fs = require('fs');
var PDFDoc = require('pdfkit');

const app = express();
app.use(fileUpLoad());
//app.use(multer());



// npm run main.js 6000 ; nodemon main.js 5000 ; node main.js 4000
app.set('port', process.argv[2] || process.env.PORT || 3000);

var server = app.listen(app.get('port'), () => {
    console.log('Express server listening at http://(' + server.address().family + ')127.0.0.1:' + server.address().address + server.address().port);
});

app.get('/images/ascii', (req, res) => {
    const asciiPath = __dirname + '/public' + req.originalUrl;
    console.log(asciiPath + '.html for get');
    res.status(200).sendFile(asciiPath + '.html');
});

app.post('/images/ascii', (req, res) => {
    const asciiPath = __dirname + '/public' + req.originalUrl;
    console.log(asciiPath + '.html for post');
    var options = {
        fit: 'box',
        width: 200,
        height: 100
    }

    if (!req.files.img) {
        return res.status(400).send('No image was uploaded.');
    }
    let imageFile = req.files.img;
    console.log(asciiPath + '.jpg');
    imageFile.mv(asciiPath + '.jpg', (err) => {
        if (err) {
            return res.status(500).send(err);
        }
        //res.status(200).sendFile(asciiPath + '.jpg');       

    });
    asciify(asciiPath + '.jpg', options, (err, asciified) => {
        if (err) console.log(err);
        console.log(asciified);
        fs.writeFileSync(asciiPath + '.txt', asciified, 'utf-8');
        // write data to file sample.html
        fs.writeFile(asciiPath + '.asc', asciified, 'ascii',
            (err) => {
                if (err) throw err;
                // if no error
                console.log("Data is written to file successfully.")
            });
        //res.status(200).set('Content-Type', 'text/plain').sendFile(asciiPath + '.txt');
        doc = new PDFDoc;
        doc.fontSize(8)
            .font('Courier')
            .text(asciiPath + '.pdf', { width: 200, align: 'justify' })
            .image(asciiPath + '.jpg', { width: 200 })
            .pipe(fs.createWriteStream(asciiPath + '.pdf'))
            .on('finish', () => {
                console.log('PDF closed')
            });
        //doc.pipe(res);
        doc.pipe(res);
        doc.end();
        //res.status(200).sendFile(asciiPath + '.pdf');

    });

    /*     imageToAscii(asciiPath + '.jpg', { color: true }, (err, converted) => {
            console.error(err || converted);
        }); */

});

app.get('/images', (req, res) => {
    const catdog = [
        { id: 1, name: 'cat1.png' },
        { id: 2, name: 'cat2.png' },
        { id: 3, name: 'cat3.png' },
        { id: 4, name: 'dog1.jpg' },
        { id: 5, name: 'dog2.jpg' },
        { id: 6, name: 'dog3.jpg' },
    ];
    var typeOfPic;
    var pathToImg;

    let numRet = Math.floor(Math.random() * catdog.length);

    (catdog[numRet].name.endsWith("jpg")) ?
        typeOfPic = "jpg" : typeOfPic = "png";

    var options = {
        fit: 'box',
        width: 200,
        height: 100
    }

    pathToImg = __dirname + '/public' + req.originalUrl + '/' + catdog[numRet].name;
    /*     console.log(pathToImg);
        asciify(pathToImg, options, function (err, asciified) {
            if (err) throw err;
            console.log(asciified);
        }); */


    res.status(200)

    res.format({

        'text/plain': () => {
            asciify(pathToImg, options, function (err, asciified) {
                if (err) throw err;
                console.log(asciified);
                res.send(asciified);
                //res.sendFile(__dirname +'/public' + req.originalUrl + '/' + asciified);
            });
        },

        'text/html': () => {
            res.send(`<h1>The time is ${(new Date())}
                            and a random image of type ${typeOfPic} is found at src="${req.originalUrl}/${catdog[numRet].name}"</h1><img src="${req.originalUrl}/${catdog[numRet].name}">`);
        },

        'image/jpg': () => {
            //console.log(res.get('Content-Type'));
            res.sendFile(__dirname + '/public' + req.originalUrl + '/' + catdog[numRet].name);
        },

        'image/png': () => {
            //console.log(res.get('Content-Type'));
            res.sendFile(__dirname + '/public' + req.originalUrl + '/' + catdog[numRet].name);
        },

        'application/json': () => {
            res.json({ typeOfPic: typeOfPic, picUrl: req.originalUrl + '/' + catdog[numRet].name });
        },

        'default': () => {
            res.status(406).end('Cannot return media type: ' + req.get('Accept'));
        }
    });

});



app.use(express.static(__dirname + '/public'));

app.use('/chart.html', (req, res) => {
    res.redirect('https://finance.yahoo.com/chart/%5EBSESN/#eyJpbnRlcnZhbCI6ImRheSIsInBlcmlvZGljaXR5IjoxLCJ0aW1lVW5pdCI6bnVsbCwiY2FuZGxlV2lkdGgiOjgsInZvbHVtZVVuZGVybGF5Ijp0cnVlLCJhZGoiOnRydWUsImNyb3NzaGFpciI6dHJ1ZSwiY2hhcnRUeXBlIjoibGluZSIsImV4dGVuZGVkIjpmYWxzZSwibWFya2V0U2Vzc2lvbnMiOnt9LCJhZ2dyZWdhdGlvblR5cGUiOiJvaGxjIiwiY2hhcnRTY2FsZSI6ImxpbmVhciIsInN0dWRpZXMiOnsidm9sIHVuZHIiOnsidHlwZSI6InZvbCB1bmRyIiwiaW5wdXRzIjp7ImlkIjoidm9sIHVuZHIiLCJkaXNwbGF5Ijoidm9sIHVuZHIifSwib3V0cHV0cyI6eyJVcCBWb2x1bWUiOiIjMDBiMDYxIiwiRG93biBWb2x1bWUiOiIjRkYzMzNBIn0sInBhbmVsIjoiY2hhcnQiLCJwYXJhbWV0ZXJzIjp7ImhlaWdodFBlcmNlbnRhZ2UiOjAuMjUsIndpZHRoRmFjdG9yIjowLjQ1LCJjaGFydE5hbWUiOiJjaGFydCJ9fX0sInBhbmVscyI6eyJjaGFydCI6eyJwZXJjZW50IjoxLCJkaXNwbGF5IjoiXkJTRVNOIiwiY2hhcnROYW1lIjoiY2hhcnQiLCJ0b3AiOjB9fSwic2V0U3BhbiI6e30sImxpbmVXaWR0aCI6Miwic3RyaXBlZEJhY2tncm91ZCI6dHJ1ZSwiZXZlbnRzIjp0cnVlLCJjb2xvciI6IiMwMDgxZjIiLCJzeW1ib2xzIjpbeyJzeW1ib2wiOiJeQlNFU04iLCJzeW1ib2xPYmplY3QiOnsic3ltYm9sIjoiXkJTRVNOIn0sInBlcmlvZGljaXR5IjoxLCJpbnRlcnZhbCI6ImRheSIsInRpbWVVbml0IjpudWxsLCJzZXRTcGFuIjp7fX1dfQ%3D%3D');
});

app.use((req, res, next) => {
    res.redirect('/error.html');
});


/*
// This responds with "Hello World" on the homepage
app.get('/', function (req, res) {
    console.log("Got a GET request for the homepage");
    res.send('/index.html');
})

// This responds a POST request for the homepage
app.post('/', function (req, res) {
    console.log("Got a POST request for the homepage");
    res.send('Hello POST');
})

// This responds a DELETE request for the /del_user page.
app.delete('/del_user', function (req, res) {
    console.log("Got a DELETE request for /del_user");
    res.send('Hello DELETE');
})

// This responds a GET request for the /list_user page.
app.get('/list_userec40d13', function (req, res) {
    console.log("Got a GET request for /list_user");
    res.send('Page Listing');
})

// This responds a GET request for abcd, abxcd, ab123cd, and so on
app.get('/ab*cd', function (req, res) {
    console.log("Got a GET request for /ab*cd");
    res.send('Page Pattern Match');
})

var server = app.listen(8081, function () {

   var host = server.address().address
   var port = server.address().port

   console.log("Example app listening at http://%s:%s", host, port)
})
*/