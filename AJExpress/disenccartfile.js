
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
app.use(cors());
//app.use(bodyParser.json()); // for parsing application/json

fs = require('fs');
function rot13(s) {
    return s.replace(/[A-Za-z]/g, function (c) {
      return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".charAt(
             "NOPQRSTUVWXYZABCDEFGHIJKLMnopqrstuvwxyzabcdefghijklm".indexOf(c)
      );
    } );
  }

var found = false;
toRegister = { userName: 'Please sign up first.', contents: [], savedTime: 'You have not signed up!' };
// npm run main.js 6000 ; nodemon main.js 5000 ; node main.js 4000
app.set('port', process.argv[2] || process.env.PORT || 3000);
var server = app.listen(app.get('port'), () => {
    console.log('Express server listening at http://(' + server.address().family + ')127.0.0.1:' + server.address().address + server.address().port);
});
app.get('/api/cart', (req, res) => {
    let userCarts = require('./encusercarts.json');
    console.log(userCarts);

    var toJSON = null;
    var fill = { userName: '', contents: [], savedTime: '' };
    found = false;
    console.log(req.query.uN);
    console.log(req.query.signU);
    for (let i of userCarts) {
        if (req.query.uN == rot13(i.userName)) {
            toJSON = i;
            found = true;
        }
    }
    if (req.query.signU == 'false') {
        (!found) ? toJSON = toRegister : toJSON;
    }
    else if (req.query.signU == 'true') {
        if (!found) {
            fill.userName = rot13(req.query.uN);
            fill.savedTime = 'Thank you for signing up!';
            userCarts.push(fill);
            fs.writeFile("./encusercarts.json", JSON.stringify(userCarts), 'utf8', (err) => {
                if (err) {
                    console.log("An error occured while writing JSON Object to File.");
                    return console.log(err);
                }

                console.log("JSON file has been saved.");
            });
            toJSON = fill;
        }
        else {
            fill.userName = rot13(req.query.uN);
            fill.savedTime = 'You have registered already!';
            toJSON = fill;
        }
    }
    //(toJSON)? toJSON : toJSON = toRegister;
    res.status(200).json(toJSON);
});
app.post('/api/cart', bodyParser.json(), (req, res) => {
    let userCarts = require('./encusercarts.json');
    var toJSON = null;
    found = false;
    console.log(req.body);
    for (let i of userCarts) {
        if (req.body.userName == rot13(i.userName)) {
            i.contents = req.body.contents;
            i.savedTime = new Date();
            console.log(i);
            toJSON = i;
            found = true;
        }
    }
    fs.writeFile("./encusercarts.json", JSON.stringify(userCarts), 'utf8', (err) => {
        if (err) {
            console.log("An error occured while writing JSON Object to File.");
            return console.log(err);
        }

        console.log("JSON file has been saved.");
    });
    (!found) ? toJSON = toRegister : toJSON;
    res.status(200).json(toJSON);
});
app.use(express.static(__dirname + '/public'));
app.use((req, res, next) => {
    res.redirect('/error.html');
});
